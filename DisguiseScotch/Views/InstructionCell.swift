//
//  InstructionCell.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/20/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

import UIKit
class InstructionCell: UICollectionViewCell {
    @IBOutlet private weak var lblInstruction: UILabel!

    var instruction: String? {
        didSet {
            self.lblInstruction.text = instruction!
            self.lblInstruction.sizeToFit()
        }
    }
}
