//
//  StartupCollectionViewDatasource.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/20/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

import UIKit
extension StartupViewController: UICollectionViewDataSource {
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(Constants.Storyboard.instructionCellReuseIdentifier, forIndexPath: indexPath) as! InstructionCell
        cell.instruction = instructions[indexPath.row].description
        return cell
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return instructions.count
    }
}