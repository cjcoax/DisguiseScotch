//
//  StartupCollectionViewDelegate.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/20/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

import UIKit
extension StartupViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: CGRectGetWidth(self.view.frame), height: CGRectGetHeight(collectionView.frame))
    }
}
