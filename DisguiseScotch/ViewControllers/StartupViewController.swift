//
//  ViewController.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/19/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

import UIKit

class StartupViewController: UIViewController {
    //MARK: - outlets
    @IBOutlet private weak var imgScotch: UIImageView!
    @IBOutlet private weak var imgO: UIImageView!
    @IBOutlet private weak var imgDisguise: UIImageView!
    @IBOutlet private weak var imgRama: UIImageView!
    @IBOutlet private weak var btnActions: UIButton!
    @IBOutlet private weak var dialogContainerView: UIView!
    @IBOutlet private weak var desciptionCollectionView: UICollectionView!


    @IBOutlet private weak var btnActionsTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var dialogContainerTopConstraint: NSLayoutConstraint!

    //MARK: - private properties
    private var currentMessageIndex: Int = 0

    //MARK: - view lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //set shadow for btnActions
        self.btnActions.layer.masksToBounds = false
        self.btnActions.layer.shadowColor = UIColor.blackColor().CGColor
        self.btnActions.layer.shadowOffset = CGSize(width: 0, height: -2)
        self.btnActions.layer.shadowRadius = 5
        self.btnActions.layer.shadowOpacity = 0.5

        self.btnActions.setTitle(instructions[self.currentMessageIndex].action, forState: .Normal)
        
        //animate components
        UIView.animateWithDuration(0.8,
            animations: { () -> Void in
                self.imgO.transform = CGAffineTransformMakeScale(4, 4)
                let translateTransform = CGAffineTransformMakeTranslation(0, CGRectGetHeight(self.view.frame)/5.0)
                self.imgScotch.transform = CGAffineTransformScale(translateTransform, 2, 2)
                self.imgDisguise.transform = CGAffineTransformMakeTranslation(0, -CGRectGetHeight(self.view.frame))
                self.imgRama.transform = CGAffineTransformMakeTranslation(0, CGRectGetHeight(self.view.frame))
            }) { (completed) -> Void in
                //start button and header animation in
                self.btnActionsTopConstraint.constant = 0
                self.dialogContainerTopConstraint.constant = 0

                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                    }, completion: nil)
        }
    }


    //MARK: - actions
    @IBAction private func btnActionTapped(sender: AnyObject) {
        self.currentMessageIndex++
        if self.currentMessageIndex < instructions.count {
            self.showMessageForIndex(self.currentMessageIndex)
        } else {
            goToMainScreen()
        }
    }



    //MARK: - private methods

    /**
    animates message and button caption 
    - parameter: message to show
    - parameter: button caption
    */
    private func showMessageForIndex(index: Int) {
        self.desciptionCollectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: index, inSection: 0), atScrollPosition: .CenteredHorizontally, animated: true)
        self.btnActions.setTitle(instructions[index].action, forState: .Normal)
    }

    private func goToMainScreen() {
        self.btnActionsTopConstraint.constant = -(CGRectGetHeight(self.btnActions.frame) + 20)
        self.dialogContainerTopConstraint.constant = -(CGRectGetHeight(self.dialogContainerView.frame) + 20)
        UIView.animateWithDuration(0.4, animations: { () -> Void in
            self.view.layoutIfNeeded()
            self.imgScotch.transform = CGAffineTransformMakeScale(1, 1)
            }, completion: nil)
    }
}

