//
//  Client.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/20/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

import Foundation

/**
 This class is created in absense of a robust networking framework such as AFNetworking, Alamofire, etc. It will eventually provide all api interactions such as GET/POST/multipart form data for uploading images, etc
 */
class Client {
    let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())

    /**
     super simple request that only does get with absolutely no header, qs param, or any other http header of body part.
     It makes the request on a background thread and then when the response is back march it up to the main thread
     - parameter: url to get data from
     */
    func verySimpleGetWithoutAnyHeaderOrQueryStringParameterOrAnythingElse(endpoint: NSURL, completionHandler: (NSData?, NSURLResponse?, NSError?) -> Void) {
        let request = NSMutableURLRequest(URL: endpoint)
        request.HTTPMethod = "GET"

        //go to a background thread
        let concurrentQueue = NSOperationQueue()
        concurrentQueue.addOperationWithBlock { () -> Void in
            let dataTask: NSURLSessionDataTask = self.session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
                    //go back to the main thread
                    let mainQueue = NSOperationQueue.mainQueue()
                    mainQueue.addOperationWithBlock({ () -> Void in
                        completionHandler(data, response, error)
                    })
            })

            dataTask.resume()
        }

    }
}