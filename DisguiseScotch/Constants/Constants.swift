//
//  Constants.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/19/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

struct Constants {
    struct Storyboard {
        static let introSegue = "introSegue"
        static let instructionCellReuseIdentifier = "instructionCellReuseIdentifier"
    }
}