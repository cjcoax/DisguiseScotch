//
//  Strings.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/19/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//


let instructions: [Instruction] = [
    Instruction(description: "S'up player...", action: "Say \"hello\""),
    Instruction(description: "OK. Long story short... There are some government thugs out there looking for me.", action: "Tell me more"),
    Instruction(description: "I need to help me pick a costume so that I can run to liquor store without anyone recognize me", action: "OK"),
]