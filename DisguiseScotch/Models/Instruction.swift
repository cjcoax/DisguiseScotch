//
//  Instruction.swift
//  DisguiseScotch
//
//  Created by Amir Rezvani on 3/20/16.
//  Copyright © 2016 cjcoaxapps. All rights reserved.
//

struct Instruction {
    let description: String
    let action: String

    init(description: String,
        action: String
        ) {
            self.description = description
            self.action = action
    }
}
