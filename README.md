#Disguise Scotch

##High level architecture
1. Create storyboard with objects that are needed.
2. Create all animation for the startup
3. Create a data structure to use for showing intro messages + button captions
4. Load data from the server and store it locally, so we don't need to call the api everytime we open up the app
5. place locally available thumbnail in it's place
6. add new thumbnails that we don't have locally, when the response comes back from the server
7. handle animation for thumbnail selector
8. handle expanding thumbnail to cover Scotch's body
9. handle the state where Scotch has a costume and all other thumbnails are collapsed
10. handle deselecting a costume and putting all thumbnails back to their original position






##Detailed architecture
1. Create storyboard with objects that are needed. Also create all the constraints that are needed and make sure the scenes looks fine on all supported devices

2. Create hooks to the backing storyboard, e.g. create outlets, actions,..

3. Start analyzing what animations are needed. Some animations are applicable to CALayer while others are more relevant to NSLayoutConstraint.

4. Create animations and clean up views that are not needed by calling uiViewSubclass.removeFromSuperView(). I have skipped clean up step as I needed to update constraits and time limit does not allow it

5. As soon as we load the main screen, We have to see how many costumes we have in config.plist. If they are 5 of them, it means that we have already in sync with the remote items, and there is no need to make the service call again. Otherwise, we have to make the request to the server to get the json response for all costumes. Preferably using a third party framework like AFNetworking or Alamofire, since they abstract a lot of details away, like going to background thread to send the request and coming to the main thread to refresh the UI components. When the response comes back, we have to parse the response preferably using SwiftyJSON or any other json parser. Then we have to save this the items that are missing from local config.plist and save them. 

6. Create a reusable nib file to be used for thumbnails (it contains a UIImageView for the ring and a UIImageView for the actual costume).

7. Read config.plist into an array of dictionary objects.

8. Create a function to calculate the center of each thumbnail based on display_x, display_y. Meaning we have to multiply this view controlles's view's width and height by display_x, display_y to get the center of the view.

9. As soon response comes back from the server we have to load them in and re-arrange thumbnail items. If they are already available locally, we load them as part of step 8.


10. We have to create a way to be able to spin the thumbnails. The quickest thing I can think of is to animate them along a UIBezierPath which is a circle that covers all thumbnails. Then a gesture recognizer to be able to move items. 

11. Create a function that calculates center of selected costume to put it on Scotch. it should take alignment_x and alignment_y and multiply it by this view controlles's view's width and height. We use this function to calculate center of the costume.

12. Add a UISwipeGestureRecognizer to the full costume so the user should be able to swipe to unclothed Scotch.

12. Once user taps on the thumbnail, we should use UIDynamics' UICollisionBehavior and add it to all thumbnails. 

13. If user swipe the full costume, we need to call the the function we've created in step 11.

